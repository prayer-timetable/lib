module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          node: 'current',
        },
        // modules: false,
      },
    ],
    '@babel/typescript',
    [
      'minify',
      {
        builtIns: false,
        evaluate: false,
        mangle: false,
      },
    ],
  ],
  plugins: ['@babel/proposal-class-properties', '@babel/proposal-object-rest-spread'],
  ignore: ['builtins'],
}
