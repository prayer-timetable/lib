# About

This package serves as a convenient way to display your daily prayers, both for individual use or for masjids. This library is not for automatic calculation of prayer times based on location. Based on my experience, 'automatic' prayer apps do not display exactly the same time as local masjids, which comes to be an issue if determining exact time the prayer commences or your fast begins and ends.

You need to provide 2 inputs, one with prayer times and second with settings. More details below. The lib itself exports 2 functions, namely prayersCalc() and dayCalc().

## Installation - Yarn:
    yarn add prayer-timetable-lib

## Installation - NPM:
    npm i prayer-timetable-lib


## Usage:
    import { prayersCalc, dayCalc } from 'prayer-timetable-lib'

## Prayers Calculation
The prayersCalc() function takes following arguments:
    prayersCalc(timetable, settings, showJamaah, city)

* timetable: timetable object / json file in the format as shown below. Defaults to included timetable json file.
* settings: settings object as discussed below. Defaults to included generic settings json file.
* show: boolean value (true/false) to display jamaah times. Default is true.
* city: used for daylight saving calculation, as in [time zones list]( https://www.geoips.com/en/resources/timezones/list-of-timezones). Defaults to Europe/Dublin.

The function returns the following:

    {
        prayers: {
            today: [], // returns list of today's prayers, see below
            yesterday: [], // list of yesterday's prayers, see below
            tomorrow [], // list of tomorrow's prayers, see below
        },
        previous, // returns single item from prayer list, relative to now. Moment object.
        current, // as above, current
        next, // as above, next
        focus, // it is next prayer/jamaah in focus; if the jamaah is pending it will be jamaah time object, otherwise it is same as 'next' above
        countUp: {
            name, // string, name of current prayer
            time, // moment object, time of current prayer
            duration // moment duration object, time since last prayer (or jamaah time if on)
        }
        countDown: {name, time, duration}, // as above, refers to next prayer / jamaah
        now, // moment() object - time now
        hijri, // as above but incorporates hijri day adjustment
        percentage, // moment duration object, percentage of time passed from previous prayer/jamaah to next prayer/jamaah.
        isAfterIsha, // boolean, shows if current time is after isha prayer/jamaah
    }

Prayer list above (there are six total - fajr, shurooq, dhuhr, asr, maghrib, isha; 0-5), each one returning the following:

    {
        time, // moment object, time of the prayer
        isJamaahPending, // boolean, shows if jamaah is next
        jtime, // moment object, time of the jamaah
        index, // number of prayer (ie. 0 for fajr)
        hasPassed, // boolean, if the prayer is in the past
        name, // string, name of the prayer
        when, // string, today/tomorrow
        dstAdjust // number, daylight saving time adjustment -1, 0, 1
    }


## Day Calc

This is just a helper for some commonly used moment objects. It takes 4 arguments with displayed defaults:

    dayCalc(offsetDay = 0, offSetHour = 0, hijrioffset = 0, city = 'Europe/Dublin')


## Timetable File

Typically, you will have your timetable file in json format and import/conver to object such as this:

    import timetable from './timetable.json'

It is structured as follows:

    {
        "1": {
            "1": [[6,43],[8,37],[12,30],[14,3],[16,19],[18,7]],
            "2": [[6,43],[8,37],[12,31],[14,3],[16,20],[18,8]],
            "3": [[6,43],[8,37],[12,31],[14,4],[16,22],[18,9]],
            ...
        }
    ...
    }

Starts with month (first "1"), followed by day in a month ("1", "2", "3", ...). Every day contains array of six sub-arrays with [hour:minute] (no zero prepend) displayed inside.

## Settings file

Settings file would have the following minimum structure:

    {
        "hijrioffset": "1",
        "join": "0",
        "jummuahtime": [13,15],
        "taraweehtime": [23,15],
        "jamaahmethods": ["beforenext","","beforenext","afterthis","afterthis","fixed"],
        "jamaahoffsets": [[0,30],[],[0,15],[0,15],[0,15],[22,25]],
    }

* hijrioffset: adjust the hijri date, ie. 0, 1, -1
* join: 0/1, should maghrib and isha be prayed together in congregation
* jummuahtime: array, friday prayer time; not currently used by lib, but by other apps using it
* taraweehtime: array, taraweeh prayer time; not currently used by lib, but by other apps using it
* jamaahmethods: array of strings to calculate start of congregation prayer - "beforenext" - hours and minutes before next prayer (ie. when fajr is prayed 30 minutes before sun rise); "afterthis" - hours and minutes after adhan; "fixed" - fixed time for the prayer (H:m)
* jamaahoffsets - array of sub-arrays of numbers, used for methods above


## Demo

Clone the repo and run demo app:

    git clone https://gitlab.com/prayer-timetable/prayer-timetable-lib.git
    cd prayer-timetable-lib
    yarn
    yarn demo

Demo looks like this:

    26/Nov/2018 19/Rab-I/1440 dst: 0 isafterIsha: false
    
    today    0 - fajr        06:14  07:35
    today    1 - shurooq     08:05  08:05
    today    2 - dhuhr       12:14  13:44
    today    3 - asr         13:59  14:14
    today    4 - maghrib     16:20  16:35 next
    today    5 - isha        18:04  22:25
    
    asr jamaah 14:14 1:22:23 << 15:36:23 >> 0:43:37 maghrib 16:20
    
    █████████████████████████████████████############# 65.38

Use the provided code as you wish. You can, for example, when isha time passes, switch to tomorrow's prayer list. 

    import timetable from './timetable'
    import settings from './settings'
    const { prayers, isAfterIsha } = prayersCalc(timetable, settings)

    const prayerList = isAfterIsha ? prayers.tomorrow : prayers.today
  
    prayerList.forEach((element) => {
        console.log(
            element.when, '\t',
            element.index, \t,
            element.time.format('HH:mm'), \t, 
            element.jtime.format('HH:mm'), \t,
            element.name === next.name ? 'next' '', \t,
            element.isJamaahPending ? 'jamaah pending' : ''
        )
    }


## Other

This lib is used by prayer-timetable-react and other packages for convenient way to keep the prayer calculations separate from the design logic.